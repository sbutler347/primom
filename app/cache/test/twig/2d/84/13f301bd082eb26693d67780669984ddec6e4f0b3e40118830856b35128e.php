<?php

/* ::base.html.twig */
class __TwigTemplate_2d8413f301bd082eb26693d67780669984ddec6e4f0b3e40118830856b35128e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>";
        // line 13
        $this->displayBlock('navigation', $context, $blocks);
        // line 21
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 23
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "\t\t    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t\t<link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/searchForm.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

\t\t";
    }

    // line 13
    public function block_navigation($context, array $blocks = array())
    {
        // line 14
        echo "    <nav>
        <ul class=\"navigation\">
            <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><img src='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logotab.jpg"), "html", null, true);
        echo "'></img></a></li>

        </ul>
    </nav>
";
    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
    }

    // line 22
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 22,  95 => 21,  84 => 16,  77 => 13,  62 => 6,  56 => 5,  47 => 22,  42 => 13,  34 => 6,  24 => 1,  80 => 14,  75 => 17,  70 => 8,  65 => 7,  59 => 12,  53 => 8,  50 => 23,  44 => 21,  39 => 4,  36 => 11,  30 => 5,);
    }
}

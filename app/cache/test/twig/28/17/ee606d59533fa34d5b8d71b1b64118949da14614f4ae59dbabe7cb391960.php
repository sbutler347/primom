<?php

/* strathPrimomBundle:Search:basic.html.twig */
class __TwigTemplate_2817ee606d59533fa34d5b8d71b1b64118949da14614f4ae59dbabe7cb391960 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/search.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Search:basic";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo " <form action=\"";
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_basic");
        echo "\" method=\"get\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " class=\"homeSearch\">
        ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

        ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "searchString"), 'row');
        echo "

        ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        <input type=\"submit\" value=\"Submit\" />
    </form>
\t<br/>
\t<div id=\"results\">
";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["collText"]) ? $context["collText"] : $this->getContext($context, "collText")), "html", null, true);
        echo "<br/><br/>
";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["results"]) ? $context["results"] : $this->getContext($context, "results")));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            echo " 

<div id=\"content\">
\t<div id=\"inner\">
\t<ul><li class=\"itemTitle\"><a href=\"http://suprimo.lib.strath.ac.uk/primo_library/libweb/action/display.do?tabs=detailsTab&ct=display&fn=search&doc=";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "recordid"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "title"), "html", null, true);
            echo "</a></li>
\t<li>Author: ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "creator"), "html", null, true);
            echo "</li>
\t<li>Pub: ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "publisher"), "html", null, true);
            echo "</li>
\t<li>Date: ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "creationDate"), "html", null, true);
            echo "</li>
\t<li>Format: ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "format"), "html", null, true);
            echo "</li>
\t";
            // line 31
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "version")) > 0)) {
                // line 32
                echo "\t\t<li>Version: ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "version"), "html", null, true);
                echo "</li></ul><br/>
\t";
            }
            // line 34
            echo "\t</div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Search:basic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 37,  122 => 34,  116 => 32,  114 => 31,  110 => 30,  106 => 29,  102 => 28,  98 => 27,  92 => 26,  83 => 22,  79 => 21,  70 => 15,  65 => 13,  60 => 11,  53 => 10,  50 => 9,  44 => 7,  38 => 4,  33 => 3,  30 => 2,);
    }
}

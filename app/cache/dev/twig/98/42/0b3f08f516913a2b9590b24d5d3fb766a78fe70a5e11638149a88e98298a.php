<?php

/* strathPrimomBundle:Account:requests.html.twig */
class __TwigTemplate_98420b3f08f516913a2b9590b24d5d3fb766a78fe70a5e11638149a88e98298a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/account.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Account:main";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "<ul class=\"navigation\">
\t\t\t";
        // line 9
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "

        </ul>
\t\t";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "
<h2 class='accTitle'>Requests</h2>
<div class='uname'>";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "</div>
<div class='spc'></div>
<div class=\"accNav\"><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\"><span id='nlink'>Back</span></a></div>

<div id=\"content\">

";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["requests"]) ? $context["requests"] : $this->getContext($context, "requests")));
        foreach ($context['_seq'] as $context["_key"] => $context["request"]) {
            // line 23
            echo "\t<div id=\"inner\">
<ul>
\t<li class='itemTitle'>Title:";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "title"), "html", null, true);
            echo "</li>
\t<li class='item'><span class='itemLabel'>Expiry Date: </span> ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "expDate"), "html", null, true);
            echo "</li>
\t<li class='item'><span class='itemLabel'>Pick up location: </span> ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "pickuplocation"), "html", null, true);
            echo "</li>
</ul>
\t</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['request'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Account:requests.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 32,  100 => 27,  96 => 26,  92 => 25,  88 => 23,  84 => 22,  77 => 18,  72 => 16,  68 => 14,  65 => 13,  57 => 9,  54 => 8,  51 => 7,  45 => 6,  39 => 4,  34 => 3,  31 => 2,);
    }
}

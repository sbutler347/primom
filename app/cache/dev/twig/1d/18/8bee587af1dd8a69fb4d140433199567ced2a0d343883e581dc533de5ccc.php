<?php

/* strathPrimomBundle:Login:submit.html.twig */
class __TwigTemplate_1d188bee587af1dd8a69fb4d140433199567ced2a0d343883e581dc533de5ccc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Login";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "
<ul class=\"navigation\">
\t\t\t";
        // line 10
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "
        </ul>
\t\t";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "<div class='uname'>";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Login:submit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 14,  65 => 13,  58 => 10,  54 => 8,  51 => 7,  45 => 6,  39 => 4,  34 => 3,  31 => 2,);
    }
}

<?php

/* strathPrimomBundle:Account:available.html.twig */
class __TwigTemplate_5988bbb6446049ee4c3c720e27f1e4cbddea6481c919f5ffc5834006610f7f91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/account.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Account:main";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "<ul class=\"navigation\">
            <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><img src='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logotab.gif"), "html", null, true);
        echo "'></img></a></li>
\t\t\t<li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\">Home</a></li>
            <li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_login");
        echo "\">Login</a></li>
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_logout");
        echo "\">Logout</a></li>
            <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\">My Account</a></li>
        </ul>
\t\t";
    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        // line 17
        echo "<div class=\"accMenuItem\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\">Back to my account</a></div>
";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["available"]) ? $context["available"] : $this->getContext($context, "available")), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Account:available.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 18,  85 => 17,  82 => 16,  75 => 13,  71 => 12,  67 => 11,  63 => 10,  57 => 9,  54 => 8,  51 => 7,  45 => 6,  39 => 4,  34 => 3,  31 => 2,);
    }
}

<?php

/* ::base.html.twig */
class __TwigTemplate_2d8413f301bd082eb26693d67780669984ddec6e4f0b3e40118830856b35128e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'strathTag' => array($this, 'block_strathTag'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
\t";
        // line 16
        $this->displayBlock('navigation', $context, $blocks);
        // line 28
        echo "    ";
        $this->displayBlock('body', $context, $blocks);
        // line 30
        echo "  ";
        $this->displayBlock('footer', $context, $blocks);
        // line 35
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 76
        echo "\t
    </body>
</html>


";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "\t\t\t<link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t\t\t<link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/searchForm.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t\t\t<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.mobile.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t\t\t<link rel=\"stylesheet\" href=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css\" />
\t\t";
    }

    // line 16
    public function block_navigation($context, array $blocks = array())
    {
        // line 17
        echo "    <nav>
        <ul class=\"navigation\">
\t\t\t\t<li class=\"logo\"><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><img src='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logotab.gif"), "html", null, true);
        echo "'></img></a></li>
\t\t\t\t<li class=\"pgNav\"><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><span id='nlink'>Home</span></a></li>
\t\t\t\t<li class=\"pgNav\"><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_login");
        echo "\"><span id='nlink'>Login</span></a></li>
\t\t\t\t<li class='pgNav'><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_logout");
        echo "\"><span id='nlink'>Logout</span></a></li>
\t\t\t\t<li class=\"pgNav\"><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\"><span id='nlink'>My Account</span></a></li>
     
        </ul>
    </nav>
\t";
    }

    // line 28
    public function block_body($context, array $blocks = array())
    {
        $this->displayBlock('strathTag', $context, $blocks);
    }

    public function block_strathTag($context, array $blocks = array())
    {
        echo "<h3 class='UniTag'>University of Strathclyde Library</h3>";
    }

    // line 30
    public function block_footer($context, array $blocks = array())
    {
        // line 31
        echo "  \t<div id='result'></div>

  <span class='fullSiteContainer'><span class='fullSiteLink'><a href='http://suprimo.lib.strath.ac.uk'>Switch to Full Site</a></span></span>
  ";
    }

    // line 35
    public function block_javascripts($context, array $blocks = array())
    {
        // line 36
        echo "\t\t<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
\t\t<script src=\"//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.1/jquery.mobile.min.js\"></script>
\t\t<script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js\"></script>

\t\t

\t\t<script>
\t\t\$(document).ready(function(){
\t\t\$.mobile.ajaxEnabled = false
\t\t\t\$('.pgNav').mouseenter(function(){
\t\t\t\t\$(this).css('background-color','#2b69ae');
\t\t\t\t});
\t\t\t\$('.pgNav').mouseleave(function(){
\t\t\t\t\$(this).css('background-color','#002b5c');
\t\t\t\t});
\t\t\t\$('.page>a , .next>a, .last>a, .previous>a, .first>a').mouseenter(function(){
\t\t\t\t\$(this).css('color','#4e7bae');
\t\t\t\t});
\t\t\t\$('.page>a , .next>a, .last>a, .previous>a, .first>a').mouseleave(function(){
\t\t\t\t\$(this).css('color','#ffffff');
\t\t\t\t});
\t\t\t\t
\t\t\tvar bgId = parseInt(Math.random()*4, 11) + 1;
\t\t\tif(bgId == 1){
\t\t\t\t\$('body').css('background-image','url(/primom/web/images/12-003-46.jpg)');
\t\t\t\t}
\t\t\tif(bgId == 2){
\t\t\t\t\$('body').css('background-image','url(/primom/web/images/12-059-84.jpg)');
\t\t\t\t}
\t\t\tif(bgId == 3){
\t\t\t\t\$('body').css('background-image','url(/primom/web/images/12-003-42.jpg)');
\t\t\t\t}
\t\t\tif(bgId == 4){
\t\t\t\t\$('body').css('background-image','url(/primom/web/images/12-003-43.jpg)');
\t\t\t\t}

\t\t\t});
\t\t</script>
\t\t
\t";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 36,  147 => 35,  140 => 31,  137 => 30,  126 => 28,  117 => 23,  113 => 22,  109 => 21,  105 => 20,  99 => 19,  95 => 17,  92 => 16,  85 => 9,  81 => 8,  76 => 7,  73 => 6,  67 => 5,  58 => 76,  55 => 35,  52 => 30,  49 => 28,  47 => 16,  41 => 13,  38 => 12,  36 => 6,  32 => 5,  26 => 1,);
    }
}

<?php

/* strathPrimomBundle:Account:main.html.twig */
class __TwigTemplate_8865d19d1754853fbf725c2f53e9ead921805c201e05a8c0542d557039b4f9d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'strathTag' => array($this, 'block_strathTag'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/account.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Account:main";
    }

    // line 9
    public function block_navigation($context, array $blocks = array())
    {
        // line 10
        echo "<ul class=\"navigation\">
\t\t\t";
        // line 11
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "
\t\t\t</ul>
\t\t";
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "
";
        // line 16
        $this->displayBlock('strathTag', $context, $blocks);
        // line 17
        echo "
<div class='uname'>";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "</div>
<div class='spc'></div>
<div class=\"accNav\"><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_items");
        echo "\"><span id='nlink'>Items Loaned</span></a></div>
<div class=\"accNav\"><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_fines");
        echo "\"><span id='nlink'>Fines and Fees</span></a></div>
<div class=\"accNav\"><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_available");
        echo "\"><span id='nlink'>Items Available</span></a></div>
<div class=\"accNav\"><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_requests");
        echo "\"><span id='nlink'>Requests</span></a></div>

";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "    <div class='flashMessage'> ";
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 16
    public function block_strathTag($context, array $blocks = array())
    {
        echo "<h3 class='accTitle'>My Account</h3>";
    }

    // line 29
    public function block_javascripts($context, array $blocks = array())
    {
        // line 30
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t\t<script>
\t\t\$(document).ready(function(){
\t\t\$.mobile.ajaxEnabled = false
\t\t\t\$('.accNav').mouseenter(function(){
\t\t\t\t\$(this).css('background-color','#bfbebb');
\t\t\t\t});
\t\t\t\$('.accNav').mouseleave(function(){
\t\t\t\t\$(this).css('background-color','#a7a9ac');
\t\t\t\t});

\t\t\t});
\t\t</script>
\t\t";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Account:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 30,  125 => 29,  119 => 16,  108 => 26,  104 => 25,  99 => 23,  95 => 22,  91 => 21,  87 => 20,  82 => 18,  79 => 17,  77 => 16,  74 => 15,  71 => 14,  64 => 11,  61 => 10,  58 => 9,  52 => 8,  45 => 5,  41 => 4,  36 => 3,  33 => 2,);
    }
}

<?php

/* strathPrimomBundle:Search:basic.html.twig */
class __TwigTemplate_2817ee606d59533fa34d5b8d71b1b64118949da14614f4ae59dbabe7cb391960 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'strathTag' => array($this, 'block_strathTag'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/search.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
\t\t\t <link rel=\"stylesheet\" href=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css\" />

\t";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo "Search Results";
    }

    // line 10
    public function block_navigation($context, array $blocks = array())
    {
        // line 11
        echo "
<ul class=\"navigation\">
\t\t ";
        // line 13
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "        
        </ul>
\t\t";
    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        // line 17
        $this->displayBlock('strathTag', $context, $blocks);
        // line 18
        echo "
 <form action=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_basic");
        echo "\" method=\"get\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " class=\"homeSearch\">
        ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "searchString"), 'row');
        echo "

        ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        <input type=\"submit\" value=\"Submit\" />
    </form>
\t<div id=\"dialog\"></div>
\t<div id=\"results\">
\t<div class='spc'></div>
\t\t<div class='totalRes'>";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["collText"]) ? $context["collText"] : $this->getContext($context, "collText")), "html", null, true);
        echo "</div>
\t<div class=\"navigation\">
\t\t";
        // line 32
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo "
\t</div>
\t";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            echo " 
\t\t<div id=\"content\">
\t\t\t<div id=\"inner\">
\t\t\t<ul>
\t\t\t\t";
            // line 38
            if ((!(null === $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "recordid", array(), "array")))) {
                // line 39
                echo "\t\t\t\t\t<li><span class='itemTitle'><a class=\"fRecordLink\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "recordid"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "title"), "html", null, true);
                echo "</a></span></li>
\t\t\t\t\t<li><span class='resultLabel'>Location:</span><span class='resultText'>In Library</span></li>
\t\t\t\t";
            }
            // line 42
            echo "\t\t\t\t";
            if ((null === $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "recordid", array(), "array"))) {
                // line 43
                echo "\t\t\t\t\t<li><span class='itemTitle'><a class=\"centralLink\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "outLink"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "title"), "html", null, true);
                echo "</a></span></li>
\t\t\t\t\t<li><span class='resultLabel'>Location:</span><span class='resultText'>Online</span></li>
\t\t\t\t";
            }
            // line 46
            echo "\t\t\t\t
\t\t\t\t
\t\t\t<!--\t<li class=\"itemTitle\">";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "title"), "html", null, true);
            echo "</li> -->
\t\t\t\t
\t\t\t\t<li><span class='resultLabel'>Type:</span><span class='resultText'> ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "type"), "html", null, true);
            echo "</span></li>
\t\t\t\t<li><span class='resultLabel'>Author: </span><span class='resultText'>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "creator"), "html", null, true);
            echo "</span></li>
\t\t\t\t";
            // line 52
            if ((!(null === $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "publisher", array(), "array")))) {
                // line 53
                echo "\t\t\t\t\t<li><span class='resultLabel'>Pub: </span><span class='resultText'>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "publisher"), "html", null, true);
                echo "</span></li>
\t\t\t\t";
            }
            // line 55
            echo "\t\t\t\t";
            if ((null === $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "publisher", array(), "array"))) {
                // line 56
                echo "\t\t\t\t\t<li><span class='resultLabel'>Collection: </span><span class='resultText'>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "ispartof"), "html", null, true);
                echo "</span></li>
\t\t\t\t";
            }
            // line 58
            echo "\t\t\t\t";
            if ((!(null === $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "creationDate", array(), "array")))) {
                // line 59
                echo "\t\t\t\t\t<li><span class='resultLabel'>Date:</span><span class='resultText'> ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "creationDate"), "html", null, true);
                echo "</span></li>
\t\t\t\t";
            }
            // line 61
            echo "\t\t\t\t";
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "version")) > 0)) {
                // line 62
                echo "\t\t\t\t\t<li><span class='resultLabel'>Version: </span><span class='resultText'>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "version"), "html", null, true);
                echo "</span></li>
\t\t\t\t";
            }
            // line 64
            echo "
\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "\t<div class=\"navigation\">
\t\t";
        // line 70
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo "
\t</div>
</div>
";
    }

    // line 17
    public function block_strathTag($context, array $blocks = array())
    {
        echo "<h3 class='accTitle'>Catalogue Search</h3>";
    }

    // line 74
    public function block_javascripts($context, array $blocks = array())
    {
        // line 75
        echo "\t\t ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

\t<script>
\t\t\$(document).ready(function(){
\t\t\t\$('.fRecordLink').click(function(e){
\t\t\t\te.preventDefault();
\t\t\t\tvar link = \$(this).attr('href');
\t\t\t\tvar path = \"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_basic_full");
        echo "\";
\t\t\t\tvar url = path+\"/\"+link;
\t\t\t\t\$.ajax({
\t\t\t\t\ttype: \"GET\",
\t\t\t\t\turl: url,
\t\t\t\t\tcontentType: 'application/json',
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(response){
\t\t\t\t\t\tvar parsedData = JSON.parse(response);
\t\t\t\t\t\tvar type =(parsedData.xml['type']);
\t\t\t\t\t\tvar title =(parsedData.xml['title']);
\t\t\t\t\t\tvar creator =(parsedData.xml['creator']);
\t\t\t\t\t\tvar publisher =(parsedData.xml['publisher']);
\t\t\t\t\t\tvar edition =(parsedData.xml['edition']);
\t\t\t\t\t\tvar format =(parsedData.xml['format']);
\t\t\t\t\t\tvar subject =(parsedData.subject);
\t\t\t\t\t\tvar creationdate =(parsedData.xml['creationdate']);
\t\t\t\t\t\t\$('#dialog').html(\"<span class='dialogLabel'>Type:  </span> <span class='dialogContent'>\"+type+\"</span><br/><span class='dialogLabel'>Title:  </span>  <span class='dialogContent'>\"+title+\"</span><br/><span class='dialogLabel'>Creator: </span> <span class='dialogContent'>\"+creator+\"</span><br/><span class='dialogLabel'>Publisher: </span> <span class='dialogContent'>\"+publisher+\"</span><br/><span class='dialogLabel'>Edition: </span> <span class='dialogContent'>\"+edition+\"</span><br/><span class='dialogLabel'>Format: </span> <span class='dialogContent'>\"+format+\"</span><br/><span class='dialogLabel'>Subjects: </span> <br/><span class='dialogContent'>\"+subject+\"</span><br/><span class='dialogLabel'>Creation Date:</span> <span class='dialogContent'>\"+creationdate+\"</span>\").dialog({
\t\t\t\t\t\t\t\tmodal:true,
\t\t\t\t\t\t\t\ttitle: title,
\t\t\t\t\t\t\t\twidth:300,
\t\t\t\t\t\t\t\t buttons: [{ 
\t\t\t\t\t\t\t\t\ttext: \"Ok\", 
\t\t\t\t\t\t\t\t\tclick: function() { 
\t\t\t\t\t\t\t\t\t\$( this ).dialog( \"close\" ); 
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t}]
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t);
\t\t\t\t\t\t},
\t\t\t\t\terror: function(e){
\t\t\t\t\t\talert(\"Error!\");
\t\t\t\t\t\t}
\t\t\t\t\t})
\t\t\t\t});
\t\t\t});
\t\t
\t\t\t</script>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Search:basic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 82,  226 => 75,  223 => 74,  217 => 17,  209 => 70,  206 => 69,  196 => 64,  190 => 62,  187 => 61,  181 => 59,  178 => 58,  172 => 56,  169 => 55,  163 => 53,  161 => 52,  157 => 51,  153 => 50,  148 => 48,  144 => 46,  135 => 43,  132 => 42,  123 => 39,  121 => 38,  112 => 34,  107 => 32,  102 => 30,  92 => 23,  87 => 21,  83 => 20,  77 => 19,  74 => 18,  72 => 17,  69 => 16,  62 => 13,  58 => 11,  55 => 10,  49 => 9,  41 => 4,  36 => 3,  33 => 2,);
    }
}

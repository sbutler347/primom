<?php

/* strathPrimomBundle:Account:items.html.twig */
class __TwigTemplate_363dd593d8e2f910beadd41310529bfa7cffa23bc445148757d1709152841ce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'strathTag' => array($this, 'block_strathTag'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/account.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Account:main";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "<ul class=\"navigation\">
\t\t\t";
        // line 9
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "

        </ul>
\t\t";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('strathTag', $context, $blocks);
        // line 16
        echo "<div class='uname'>";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "</div>
<div class='spc'></div>
<div class=\"accNav\"><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\"><span id='nlink'>Back</span></a></div>

<div id=\"content\">

";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : $this->getContext($context, "items")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 23
            echo "\t<div id=\"inner\">
<ul>
\t<li class='itemTitle'><span>Title:</span> ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "title"), "html", null, true);
            echo "</li>
\t<li class='item'><span>Author: </span> ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "author"), "html", null, true);
            echo "</li>
\t<li class='item'><span>Collection: </span> ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "location"), "html", null, true);
            echo "</li>
\t<li class='item'><span >Due Date: </span> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "dueDate"), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "dueTime"), "html", null, true);
            echo "</li>
\t<li class='item'><span >Renewable?</span>  ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "renewable"), "html", null, true);
            echo "</li>
</ul>
\t</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "</div>
\t<br/>

";
    }

    // line 15
    public function block_strathTag($context, array $blocks = array())
    {
        echo "<h3 class='accTitle'>Items on Loan</h3>";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Account:items.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 15,  124 => 34,  113 => 29,  107 => 28,  103 => 27,  99 => 26,  95 => 25,  91 => 23,  87 => 22,  80 => 18,  74 => 16,  72 => 15,  69 => 14,  66 => 13,  58 => 9,  55 => 8,  52 => 7,  46 => 6,  40 => 4,  35 => 3,  32 => 2,);
    }
}

<?php

/* strathPrimomBundle:Home:searchlo.html.twig */
class __TwigTemplate_200a52406c1e6666e77767b76fdb55a767514458bac80850213d0bb062cdf858 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'strathTag' => array($this, 'block_strathTag'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "SUPrimoM";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/search.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

";
    }

    // line 8
    public function block_navigation($context, array $blocks = array())
    {
        // line 9
        echo "
<ul class=\"navigation\">
            ";
        // line 11
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "          
        </ul>
\t\t";
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "
\t";
        // line 16
        $this->displayBlock('strathTag', $context, $blocks);
        // line 17
        echo "
<form action=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_basic");
        echo "\" method=\"get\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " class=\"homeSearch\">
        ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "searchString"), 'row');
        echo "

        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        <input type=\"submit\" value=\"Submit\" />
    </form>
\t
";
    }

    // line 16
    public function block_strathTag($context, array $blocks = array())
    {
        echo "<h3 class='accTitle'>Catalogue Search</h3>";
    }

    // line 28
    public function block_javascripts($context, array $blocks = array())
    {
        // line 29
        echo "\t\t ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t\t ";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Home:searchlo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 29,  110 => 28,  104 => 16,  94 => 22,  89 => 20,  85 => 19,  79 => 18,  76 => 17,  74 => 16,  71 => 15,  68 => 14,  61 => 11,  57 => 9,  54 => 8,  47 => 5,  42 => 4,  39 => 3,  33 => 2,);
    }
}

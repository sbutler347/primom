<?php

/* strathPrimomBundle:Logout:logout.html.twig */
class __TwigTemplate_fb90bfb5622995f2aaf8c7c92968f48671253f2355d47ff6cb41a8a367e5e272 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Logout:logout";
    }

    // line 4
    public function block_navigation($context, array $blocks = array())
    {
        // line 5
        echo "
<ul class=\"navigation\">
            <li><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><img src='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logotab.gif"), "html", null, true);
        echo "'></img></a></li>
\t\t\t<li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\">Home</a></li>
            <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_login");
        echo "\">Login</a></li>
            <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\">My Account</a></li>
        </ul>
\t\t";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "<h1>Welcome to the Logout:logout page</h1>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Logout:logout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 14,  64 => 13,  57 => 10,  53 => 9,  49 => 8,  43 => 7,  39 => 5,  36 => 4,  30 => 3,);
    }
}

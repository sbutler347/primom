<?php

/* strathPrimomBundle:Login:login.html.twig */
class __TwigTemplate_29501d569a080682fb983d3ee2c3dc8179669579cb3d93f28d74381c23837f73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Login";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "
<ul class=\"navigation\">
\t\t\t";
        // line 10
        $this->displayParentBlock("navigation", $context, $blocks);
        echo "
        </ul>
\t\t";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "    <header>
        <h1 class=\"accTitle\">Login</h1>
    </header>
<div class='bkg'>
   <form action=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_login_submit");
        echo "\" method=\"POST\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " class='lgnForm'>
   
        ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

      <span class='lFormField'>";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "barcode"), 'row');
        echo "</span>
      <span class='lFormField'>";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "familyName"), 'row');
        echo "</span>
        ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
      <span class='lFormField'><input type=\"submit\" id=\"login\" value=\"Submit\" /></span>
    </form>
\t</div>
\t
";
    }

    // line 31
    public function block_javascripts($context, array $blocks = array())
    {
        // line 32
        echo "\t\t\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Login:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 32,  109 => 31,  99 => 25,  95 => 24,  91 => 23,  87 => 22,  82 => 20,  75 => 18,  69 => 14,  66 => 13,  59 => 10,  55 => 8,  52 => 7,  46 => 6,  40 => 4,  35 => 3,  32 => 2,);
    }
}

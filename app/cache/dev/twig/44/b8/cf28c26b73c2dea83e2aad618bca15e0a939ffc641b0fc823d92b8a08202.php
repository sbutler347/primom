<?php

/* strathPrimomBundle:Account:fines.html.twig */
class __TwigTemplate_44b8cf28c26b73c2dea83e2aad618bca15e0a939ffc641b0fc823d92b8a08202 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/account.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "strathPrimomBundle:Account:main";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "<ul class=\"navigation\">
            <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><img src='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logotab.gif"), "html", null, true);
        echo "'></img></a></li>
\t\t\t<li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\">Home</a></li>
            <li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_login");
        echo "\">Login</a></li>
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_logout");
        echo "\">Logout</a></li>
            <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\">My Account</a></li>
        </ul>
\t\t";
    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        // line 17
        echo "<div class=\"accMenuItem\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\">Back to my account</a></div>
\t<br/>
\t<h2>Fines</h2>

<div id=\"content\">

";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fines"]) ? $context["fines"] : $this->getContext($context, "fines")));
        foreach ($context['_seq'] as $context["_key"] => $context["fine"]) {
            // line 24
            echo "\t<div id=\"inner\">
<ul>
\t<li><span class='itemLabel'>Title:</span> ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fine"]) ? $context["fine"] : $this->getContext($context, "fine")), "title"), "html", null, true);
            echo "</li>
\t<li><span class='itemLabel'>Type: </span> ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fine"]) ? $context["fine"] : $this->getContext($context, "fine")), "type"), "html", null, true);
            echo "</li>
\t<li><span class='itemLabel'>Date: </span> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fine"]) ? $context["fine"] : $this->getContext($context, "fine")), "date"), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fine"]) ? $context["fine"] : $this->getContext($context, "fine")), "time"), "html", null, true);
            echo "</li>
\t<li><span class='itemLabel'>Fine Amount: </span> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fine"]) ? $context["fine"] : $this->getContext($context, "fine")), "amount"), "html", null, true);
            echo "</li>
\t<li><span class='itemLabel'>Total Due: </span>  ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fine"]) ? $context["fine"] : $this->getContext($context, "fine")), "totalAmount"), "html", null, true);
            echo "</li>
</ul>
\t</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fine'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "</div>
\t<br/>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Account:fines.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 35,  121 => 30,  117 => 29,  111 => 28,  107 => 27,  103 => 26,  99 => 24,  95 => 23,  85 => 17,  82 => 16,  75 => 13,  71 => 12,  67 => 11,  63 => 10,  57 => 9,  54 => 8,  51 => 7,  45 => 6,  39 => 4,  34 => 3,  31 => 2,);
    }
}

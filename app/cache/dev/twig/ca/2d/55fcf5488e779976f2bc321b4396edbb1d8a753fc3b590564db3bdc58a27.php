<?php

/* strathPrimomBundle:Home:searchli.html.twig */
class __TwigTemplate_ca2d55fcf5488e779976f2bc321b4396edbb1d8a753fc3b590564db3bdc58a27 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "SUPrimoM";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/searchForm.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        // line 8
        echo "
<ul class=\"navigation\">
            <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\"><img src='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logotab.gif"), "html", null, true);
        echo "'></img></a></li>
\t\t\t<li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_homepage");
        echo "\">Home</a></li>
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_login");
        echo "\">Login</a></li>
            <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_logout");
        echo "\">Logout</a></li>
            <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_account_main");
        echo "\">My Account</a></li>
        </ul>
\t\t";
    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        // line 18
        echo "
    <header>
        <h3>Welcome to the SUPrimo mobile site <br/>(work in progress)</h3>
    </header>
    <form action=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("strathPrimomBundle_basic");
        echo "\" method=\"get\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " class=\"homeSearch\">
        ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
\t\t<div class='searchLabel'>
\t\t";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "searchString"), 'label');
        echo "
\t\t</div><div>
        ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "searchString"), 'widget');
        echo "
</div>
        ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
<div class=\"formSubmit\">
        <input type=\"submit\" value=\"Submit\" />
\t\t</div>
    </form>
";
    }

    public function getTemplateName()
    {
        return "strathPrimomBundle:Home:searchli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 29,  108 => 27,  103 => 25,  98 => 23,  92 => 22,  86 => 18,  83 => 17,  76 => 14,  72 => 13,  68 => 12,  64 => 11,  58 => 10,  54 => 8,  51 => 7,  45 => 5,  40 => 4,  37 => 3,  31 => 2,);
    }
}

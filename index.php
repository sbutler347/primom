<?php
if (!isset($_GET['q'])) {

	$searchterm = "one+secret+thing"; // Default search
	
} else {
	
	$searchterm = $_GET['q'];

}
$safeSearchTerm = str_replace(" ", "+", $searchterm);

?>

<form id="search" action="index.php" method="get">
	Enter search: <input name="q" type="text" value="<?php echo $safeSearchTerm ?>">
	<input type="submit" name="submit" value="submit" />
</form>

<h1>Results for: <?php echo $safeSearchTerm ?></h1>

<?php 
$searchString= "http://suprimo.lib.strath.ac.uk:1701/PrimoWebServices/xservice/search/brief?onCampus=false&institution=SU&loc=local,scope:(SU)&query=any,contains,$safeSearchTerm&bulkSize=10&indx=1";

$data = file_get_contents("$searchString",0);
   
$sxe = simplexml_load_string($data);
	
	if ($sxe === false) {
	   echo 'Error while parsing the document';
	 //  echo '...Using offline data cache';  
	 //   $sxe = simplexml_load_file("offline.xml");
	}


// grab some info about search and display
$totalhits=	$sxe->xpath("//@TOTALHITS"); 
$firsthit= 	$sxe->xpath('//@FIRSTHIT');
$lasthit= 	$sxe->xpath('//@LASTHIT');

echo <<<searchinfo
		Viewing $firsthit[0] to $lasthit[0] of $totalhits[0]
searchinfo;

//loop, grab data from the XML and display it
foreach ($sxe->xpath('//sear:DOC') as $doc) {
	$title= 			$doc->PrimoNMBib->record->display->title;
	$creator= 		$doc->PrimoNMBib->record->display->creator;
    $publisher= 		$doc->PrimoNMBib->record->display->publisher;	    	
    $creationdate= 	$doc->PrimoNMBib->record->display->creationdate;	    	
	$format= 		$doc->PrimoNMBib->record->display->format;
	$version = 		$doc->PrimoNMBib->record->display->version;
	$frbrgroupid = 	$doc->PrimoNMBib->record->facets->frbrgroupid;

echo <<<items
	<div class="item">
		<ul>
			<li>Title: $title</li>
			<li>Creator: $creator</li>
items;
	if ($version) {
		echo "<strong>There are $version editions of this item.</strong>";
	}
	else 
	{
echo <<<items
				<ul>
					<li>Publisher: $publisher</li>
					<li>$creationdate</li>
					<li>$format</li>
				</ul>
items;
	}
echo "</ul></div>";
}
?>
<?php

namespace strath\PrimomBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use strath\PrimomBundle\Form\HomeSearch;

class HomeControllerTest extends WebTestCase
{
    public function testSearch()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
		$hs = new HomeSearch();
		$this->assertEquals('hsearch', $hs->getName());
		$this->assertTrue($crawler->filter('html:contains("Welcome to the SUPrimo mobile site")')->count() > 0);
		$this->assertTrue($crawler->filter('form:contains("Search Text")')->count() > 0);

    }

}

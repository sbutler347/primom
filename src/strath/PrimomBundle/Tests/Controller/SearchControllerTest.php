<?php

namespace strath\PrimomBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchControllerTest extends WebTestCase
{
    public function testBasic()
    {
        $client = static::createClient();
	    $crawler = $client->request('GET', '/basic?hsearch%5BsearchString%5D=orwell&hsearch%5B_token%5D=T48Qw2gno3vgEMtmOUvHtK1vmjmYRxQSqp-Ggn8qXdY');
		$this->assertTrue($crawler->filter('div:contains("Orwell")')->count() > 0);

    }

}

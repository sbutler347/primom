<?php

namespace strath\PrimomBundle\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class Login
{
    protected $barcode;
    protected $familyName;
	
    public function getBarcode()
    {
        return $this->barcode;
    }

    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    public function getFamilyName()
    {
        return $this->familyName;
    }

    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
    }
	
	public function getXml()
	{
		return '<?xml version="1.0" encoding="UTF-8"?><ser:serviceParameters xmlns:ser="http://www.endinfosys.com/Voyager/serviceParameters"><ser:parameters></ser:parameters><ser:patronIdentifier lastName="'.$this->getFamilyName().'"><ser:authFactor type="B">'.$this->getBarcode().'</ser:authFactor></ser:patronIdentifier></ser:serviceParameters>';
	}

	public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('barcode', new NotBlank());
		$metadata->addPropertyConstraint('barcode', new Length(array('min' =>10)));
        $metadata->addPropertyConstraint('familyName', new NotBlank());	
	}
}
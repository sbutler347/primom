<?php

namespace strath\PrimomBundle\Entity;

class search
{
	protected $searchString;
	
	public function getSearchString()
	{
		return $this->searchString;
	}
	
	public function setSearchString($searchString)
	{
		$this->searchString = $searchString;
	}
}
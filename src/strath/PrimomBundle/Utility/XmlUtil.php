<?php

namespace strath\PrimomBundle\Utility;

class XmlUtil{
	static function registerNamespaces($xml){
	
		$xml->registerXPathNamespace('ser', 'http://www.endinfosys.com/Voyager/serviceParameters');
		$xml->registerXPathNamespace('myac', 'http://www.endinfosys.com/Voyager/myAccount');
		$xml->registerXPathNamespace('pat', 'http://www.endinfosys.com/Voyager/patronAuthentication');

		return $xml;
		}
	}
	?>
<?php

namespace strath\PrimomBundle\Utility;

class CurlUtil{

	static function getCurl($url)
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '3');
		$resp = trim(curl_exec($ch));
		curl_close($ch);
		return $resp;
		}
	
	static function postXmlCurl($url, $xml)
	{
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		$resp = curl_exec( $ch );
		return $resp;
		}
	}
?>
<?php

namespace strath\PrimomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Snc\RedisBundle\SncRedisBundle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class LogoutController extends Controller
{
    public function logoutAction()
    {
		$request = $this->getRequest();
		$id = $request->cookies->get('PRIMO_SESSION');
		$redis = $this->container->get('snc_redis.default');
		$redis->DEL($id);
		$response = new Response();
		$response->headers->clearCookie('PRIMO_SESSION');
		$response->sendHeaders();
		return $this->redirect($this->generateUrl('strathPrimomBundle_homepage'));
    }

}

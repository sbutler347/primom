<?php

namespace strath\PrimomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use strath\PrimomBundle\Entity\Search;
use strath\PrimomBundle\Form\HomeSearch;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    public function searchAction(Request $request)
    {
		$search = new Search();
		$form = $this->createForm(new HomeSearch(), $search);
	    $request = $this->getRequest();
		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
		 return $this->redirect($this->generateUrl('strathPrimomBundle_homepage'));
			}
		}
		$pds = $request->query->get('pds_handle');
		if(isset($pds)){
			setcookie('pds_handle',$pds,time()+3600);
			return $this->render('strathPrimomBundle:Home:searchli.html.twig', array(
			'form' => $form->createView()
			));
		}
		else{
			return $this->render('strathPrimomBundle:Home:searchlo.html.twig', array(
			'form' => $form->createView()
			));
		}
    }
}
<?php

namespace strath\PrimomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use strath\PrimomBundle\Utility\CurlUtil;
use strath\PrimomBundle\Utility\XmlUtil;
use strath\PrimomBundle\Entity\Login;
use strath\PrimomBundle\Form\LoginForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Snc\RedisBundle\SncRedisBundle;

class LoginController extends Controller
{
	public function getVal($param, $val){
		$redis = $this->container->get('snc_redis.default');
		$out = $redis->HMGET($param, $val);
		return $out;
	}

    public function loginAction()
    {
		$login = new Login();
		$form = $this->createForm(new LoginForm(), $login);
		$request = $this->getRequest();
		$cookies = $request->cookies;
		if(1 != ($cookies->has('PRIMO_SESSION'))){
			if ($request->getMethod() == 'POST') {

				$form->bind($request);
				if ($form->isValid()) {
				  return $this->redirect($this->generateUrl('strathPrimomBundle_login_submit'));
				}
			}
		return $this->render('strathPrimomBundle:Login:login.html.twig', array(
			'form' => $form->createView()
			));
		}
		else{
			$id = $cookies->get('PRIMO_SESSION');
			$val= 'fName';
			$out = $this->getVal($id, $val);
			$fName = $out[0];
			return $this->render('strathPrimomBundle:Account:main.html.twig', array(
				'name' => $fName,));
		} 
    }
	
	public function getLoginXml($postArr, $primo_sess){
		$redis = $this->container->get('snc_redis.default');

		$url = 'http://pumblechook.lib.strath.ac.uk:7014/vxws/AuthenticatePatronService';
		$xml ='<?xml version="1.0" encoding="UTF-8"?><ser:serviceParameters xmlns:ser="http://www.endinfosys.com/Voyager/serviceParameters"><ser:parameters></ser:parameters><ser:patronIdentifier lastName="'.$postArr["familyName"].'"><ser:authFactor type="B">'.$postArr["barcode"].'</ser:authFactor></ser:patronIdentifier></ser:serviceParameters>';
		$resp = CurlUtil::postXmlCurl($url, $xml);
		$xml = simplexml_load_string($resp);
		$xml = XmlUtil::registerNamespaces($xml);
		$fName = (string)$xml->children('ser',true)->serviceData->children('pat',true)->fullName;
		$lName = (string)$xml->children('ser',true)->serviceData->children('pat',true)->lastName;
		$isLocal = (string)$xml->children('ser',true)->serviceData->children('pat',true)->isLocalPatron;
		$patId = $xml->children('ser',true)->serviceData->children('pat',true)->patronIdentifier->attributes();
		$patronId = $patId[0];
		
		try{
			$redis->HMSET($primo_sess,"barcode",$postArr["barcode"], 'fName', $fName, 'lName',$lName, 'local',$isLocal, 'patronId',$patronId);
			$redis->expire($primo_sess,3600);
		}
		catch(Exception $e){
			echo "Could not create user session";
		}
		$response = new Response();
		$cookie = new Cookie('PRIMO_SESSION', $primo_sess,time() + 3600, '/', null, false, false);
		$response->headers->setCookie($cookie);
		$response->sendHeaders();	
		return $fName;
	}

	public function submitAction()
	{	
		$postArr =$this->get('request')->get('login');
		$request = $this->get('request');
		$cookies = $request->cookies;
		$primo_sess = md5($postArr["barcode"]);

		if(1 === ($cookies->has('PRIMO_SESSION'))){ 
			$val = 'fName';
			$out = $this->getVal($primo_sess, $val);
			$fName = $out[0];
		}
		elseif((1 !== ($cookies->has('PRIMO_SESSION'))) && (null == $postArr =$this->get('request')->get('login'))){
			return $this->redirect($this->generateUrl('strathPrimomBundle_login'));
			}
		elseif(1 !== ($cookies->has('PRIMO_SESSION'))&& (null !== $postArr =$this->get('request')->get('login'))){
			$fName= $this->getLoginXml($postArr, $primo_sess);
			}	
		return $this->render('strathPrimomBundle:Account:main.html.twig', array(
			'name' => $fName,
			'primo_sess' => $primo_sess
		));	
	}
}
		
<?php

namespace strath\PrimomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use strath\PrimomBundle\Form\HomeSearch;
use strath\PrimomBundle\Entity\Search;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Knp\Bundle\KnpPaginatorBundle;

class SearchController extends Controller
{
    public function basicAction(Request $request)
    {
	$search = new Search();
		$form = $this->createForm(new HomeSearch(), $search);
	    $request = $this->getRequest();
		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
		 return $this->redirect($this->generateUrl('strathPrimomBundle_homepage'));
			}
		}	
		$getArr =$this->get('request')->query->all();

		$sanitisedString = $getArr['hsearch']['searchString'];
		$sanitisedString =str_replace('/','', $sanitisedString);
		$sanitisedString =str_replace(' ','+', $sanitisedString);
		$searchString= "http://suprimo.lib.strath.ac.uk:1701/PrimoWebServices/xservice/search/brief?onCampus=true&institution=SU&loc=adaptor,primo_central_multiple_fe&loc=local,scope:(SU)&query=any,contains,$sanitisedString&bulkSize=300&indx=1";

		$data = file_get_contents("$searchString",0);
		$sxe = simplexml_load_string($data);	
		if ($sxe === false) {
		   echo 'Error while parsing the document';
		}	
		$mData = $this->getXmlMData($sxe);
		$results = $this->handleXmlRecord($sxe);
		
		$paginator = $this->get('knp_paginator');
		$pagination= $paginator->paginate(
		$results,$this->get('request')->query->get('page',1)/*page number*/,5/*limit per page*/
		);
		return $this->render('strathPrimomBundle:Search:basic.html.twig', array(
	 		'form' => $form->createView(),
			'collText' => $mData['collText'],
			'pagination' => $pagination,
			'currurl' => $mData['currentUrl'],
			));
		}
	
	public function fullAction($recordid){
		$sxe = $this->loadFullRecord($recordid);
		$detail = $sxe->xpath('//PrimoNMBib:display');
		$detail = $detail[0];
		$avail = $detail->availlibrary;
		$subject = $detail->subject;
		$subject = preg_replace('/--/','<br/>', $subject);
		$avail = preg_split('/$$/',$avail);
		
		$return=array("responseCode"=>200,  "xml" =>$detail, "id" => $avail, 'subject' => $subject);
		$return = json_encode($return);
		return new JsonResponse($return,200,array('Content-Type'=>'application/json'));
	}
	
	public function loadFullRecord($recordid){
	$url= "http://suprimo.lib.strath.ac.uk:1701/PrimoWebServices/xservice/search/full?institution=SU&docId=$recordid";
		$data = file_get_contents("$url",0);
		$sxe = simplexml_load_string($data, NULL, LIBXML_NOCDATA);
		$sxe->registerXPathNamespace('sear', 'http://www.exlibrisgroup.com/xsd/jaguar/search');
		$sxe->registerXPathNamespace('PrimoNMBib', 'http://www.exlibrisgroup.com/xsd/primo/primo_nm_bib');
		if ($sxe === false) {
			echo 'Error while parsing the document';
		}
		return $sxe;
	}		
	
	public function getXmlMData($sxe){
		// grab some info about search and display
		$mData['totalhits']=	$sxe->xpath("//@TOTALHITS"); 
		$mData['firsthit']= 	$sxe->xpath('//@FIRSTHIT');
		$mData['lasthit']= 	$sxe->xpath('//@LASTHIT');
		$mData['collText']= "Found ".$mData['totalhits'][0]." records";
		$mData['currentUrl'] = $this->getRequest()->getUri();
		return $mData;
	}
	
	public function handleXmlRecord($sxe){
	//loop, grab data from the XML and display it
	foreach ($sxe->xpath('//sear:DOC') as $doc) {
		$source = $doc->PrimoNMBib->record->control->sourcesystem;
		
	 	if($source =='Voyager'){
			$result['recordid'] = $doc->PrimoNMBib->record->control->recordid;
			}  
		
		else{
			$link = $doc->children('sear',true)->LINKS->openurl;
			if($link == ''){
				$link = $doc->children('sear',true)->GETIT->attributes()->GetIt2;
				}
			$result['outLink'] = $link;
			$result['recordid'] = NULL;
			}
		$result['title']= 			$doc->PrimoNMBib->record->display->title;
		$result['creator']= 		$doc->PrimoNMBib->record->display->creator;
		$pub=$doc->PrimoNMBib->record->display->publisher;
		if($pub == ''){
			$result['publisher']= NULL;
			}
		else{
			$result['publisher']= $pub;
			}
		$date = $doc->PrimoNMBib->record->search->creationdate;	  
		if($date == ''){
			$result['creationDate']= NULL;
			}
		else{
			$result['creationDate']= $date;
			}
		$result['type']= 			$doc->PrimoNMBib->record->display->type;
		$result['version'] = 		$doc->PrimoNMBib->record->display->version;
		$result['frbrgroupid'] = 	$doc->PrimoNMBib->record->facets->frbrgroupid;
		$result['jtitle'] = 	$doc->PrimoNMBib->record->facets->jtitle;
		$result['ispartof'] = 	$doc->PrimoNMBib->record->display->ispartof;


		$results[] = $result;
		
    }	
	return $results;
	}
}


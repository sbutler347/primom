<?php

namespace strath\PrimomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use strath\PrimomBundle\Utility\CurlUtil;
use strath\PrimomBundle\Utility\XmlUtil;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Snc\RedisBundle\SncRedisBundle;
use Symfony\Component\HttpFoundation\Request;


class AccountController extends Controller
{
	public function getVal($param, $val){
		$redis = $this->container->get('snc_redis.default');
		$out = $redis->HMGET($param, $val);
		return $out;
	}

    public function mainAction()
    {
		$request = $this->getRequest();
		$cookies = $request->cookies;
		$primo_sess = $request->cookies->get('PRIMO_SESSION');
		$redis = $this->container->get('snc_redis.default');

		if(0 == ($cookies->has('PRIMO_SESSION'))){ 
			return $this->redirect($this->generateUrl('strathPrimomBundle_login'));
			}
		$out = $this->getVal($primo_sess, 'fName');
		$fName = $out[0];
		return $this->render('strathPrimomBundle:Account:main.html.twig', array(
		'name' => $fName));
	}
	
	public function checkAndGetCookie(){
		$request = $this->getRequest();
		$cookies = $request->cookies;
		$primo_sess = $request->cookies->get('PRIMO_SESSION');
		if(1 == ($cookies->has('PRIMO_SESSION'))){ 
			return $primo_sess;
			}
		else{
			return $this->redirect($this->generateUrl('strathPrimomBundle_login'));
			}
		}
	
	public function itemsAction(){
		$redis = $this->container->get('snc_redis.default');
		$loginId = $this->checkAndGetCookie();
		$action = 'items';
		$items = $this->getAccountXml($loginId,$redis, $action);

		if($items !== NULL){
			$out = $this->getVal($loginId, 'fName');
			$fName = $out[0];
			return $this->render('strathPrimomBundle:Account:items.html.twig', array(
			'items'=>$items, 
			'name' => $fName
			));
		}
		else{
			$this->get('session')->getFlashBag()->add(
			'notice',
			'You currently have no items on loan'
			);
			
		return $this->redirect($this->generateUrl('strathPrimomBundle_account_main'));
		}
			
			}
				
	public function finesAction(){
		$redis = $this->container->get('snc_redis.default');
		$loginId = $this->checkAndGetCookie();
		$action = 'fines';
		$fines = $this->getAccountXml($loginId,$redis, $action);
		$out = $this->getVal($loginId, 'fName');
		$fName = $out[0];
		if($fines !== NULL){

		return $this->render('strathPrimomBundle:Account:fines.html.twig', array(
			'fines'=>$fines,
			'name' => $fName));
		}
		else{
			$this->get('session')->getFlashBag()->add(
			'notice',
			'You currently have no outstanding fines'
			);
		return $this->redirect($this->generateUrl('strathPrimomBundle_account_main'));
		}
	}
			
	public function availableAction(){
		$redis = $this->container->get('snc_redis.default');
		$loginId = $this->checkAndGetCookie();
		$action = 'available';
		$available = $this->getAccountXml($loginId,$redis, $action);
		$out = $this->getVal($loginId, 'fName');
		$fName = $out[0];
		if($available !== NULL){
		return $this->render('strathPrimomBundle:Account:available.html.twig', array(
			'available'=>$available,
			'name' => $fName));
		}
		else{
		$this->get('session')->getFlashBag()->add(
			'notice',
			'You currently have no outstanding available items'
			);
		return $this->redirect($this->generateUrl('strathPrimomBundle_account_main'));
		}
		}
			
	public function requestsAction(){
		$redis = $this->container->get('snc_redis.default');
		$loginId = $this->checkAndGetCookie();
		$action = 'requests';
		$requests = $this->getAccountXml($loginId,$redis, $action);
		$out = $this->getVal($loginId, 'fName');
		$fName = $out[0];
		if($requests !== NULL){
		return $this->render('strathPrimomBundle:Account:requests.html.twig', array(
		'requests' => $requests,
		'name' => $fName));
		}
		else{
			$this->get('session')->getFlashBag()->add(
			'notice',
			'You currently have no outstanding requests'
			);
		return $this->redirect($this->generateUrl('strathPrimomBundle_account_main'));
		}
		}
			
	public function getAccountXml($primo_sess, $redis, $action){
		$patronId = $redis->HMGET($primo_sess, 'patronId');
		$patronId = $patronId[0];
		$url = 'http://pumblechook.lib.strath.ac.uk:7014/vxws/MyAccountService?patronId='.$patronId.'&patronHomeUbId=1@STRATHDB20020808162655';
		$resp = CurlUtil::getCurl($url);
		
 		$xml = simplexml_load_string($resp);
		$xml = XmlUtil::registerNamespaces($xml);
		
		switch($action){
			case "items":
				$accountArr = $this->getChargedItems($xml);
				break;
			case "fines":
				$accountArr = $this->getFineFees($xml);
				break;
			/* case "available":
				$accountArr = $this->getAvailable($xml);
				break; */
			case "requests":
				$accountArr = $this->getRequests($xml);
				break;
			default:
				$accountArr = NULL;
			}
			if($accountArr != NULL){
				return $accountArr;
			}
			else{
				return NULL;
			}
	}
	
	public function getFineFees($xml){
		$node = $xml->children('ser',true)->serviceData->children('myac',true)->finesFees->clusterFinesFees->fineFee;
		if($node !== NULL){
		foreach($node as $fineFee){
			$line['id'] = (string)$fineFee->fineFeeId;
			$title = explode('/',$fineFee->title);
			$line['title'] = $title[0];
			$line['type'] = (string)$fineFee->postingType;
			$fineDate = (string)$fineFee->date;
			$fineDate = explode("T",$fineDate);
			$line['date'] = $fineDate[0];
			$line['time'] = substr($fineDate[1],0,5);
			$line['amount'] = (string)$fineFee->amount->amount;
			$line['totalAmount'] = (string)$fineFee->amountTotal->amount;
			
			$fineArr[]=$line;
		}
		return $fineArr;}
		else{
		
		}
	}

	public function getAvailable($xml){
		$node = $xml->children('ser',true)->serviceData->children('myac',true)->availItems->clusterFinesFees->fineFee;
	
		foreach($node as $fineFee){
			$line['id'] = (string)$fineFee->fineFeeId;
			$title = explode('/',$fineFee->title);
			$line['title'] = $title[0];
			$line['type'] = (string)$fineFee->postingType;
			$fineDate = (string)$fineFee->date;
			$fineDate = explode("T",$fineDate);
			$line['date'] = $fineDate[0];
			$line['time'] = substr($fineDate[1],0,5);
			$line['amount'] = (string)$fineFee->amount->amount;
			$line['totalAmount'] = (string)$fineFee->amountTotal->amount;
			
			$fineArr[]=$line;
		}
		return $fineArr;
	}
	
	public function getRequests($xml){
		$node = $xml->children('ser',true)->serviceData->children('myac',true)->requests->requestItem;
	
		foreach($node as $request){
			$title = explode('/',$request->itemTitle);
			$line['title'] = $title[0];
			$expDate = (string)$request->expireDate;
			$expDate = explode("T",$expDate);
			$line['expDate'] = $expDate[0];
			$line['expTime'] = substr($expDate[1],0,5);
			$line['pickuplocation'] = (string)$request->pickuplocation;
			
			$reqArr[]=$line;
		}
		return $reqArr;
	}
	
	public function getChargedItems($xml){
	
		$node = $xml->children('ser',true)->serviceData->children('myac',true)->chargedItems->clusterChargedItems->chargedItem;
		foreach($node as $item){
		
			$title = explode('/',$item->title);
			$line['title']= $title[0];
			$line['author']= (string)$item->author;
			$line['location']= (string)$item->location;
			$dueDateTime = (string)$item->dueDate;
			$dueDateTime = explode("T", $dueDateTime);
			$line['dueDate'] = $dueDateTime[0];
			$line['dueTime'] = substr($dueDateTime[1],0,5);
			$renewableBool = (string)$item->renewable;
			if($item->renewable == "true"){
				$line['renewable']= 'Yes';}
			else{
				$line['renewable']= 'No';
				}
			$accountArr[]=$line;
		}
		return $accountArr;
	}
}

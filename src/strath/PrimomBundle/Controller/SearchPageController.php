<?php

namespace strath\PrimomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use strath\PrimomBundle\Entity\Search;
use strath\PrimomBundle\Form\HomeSearch;
use Symfony\Component\HttpFoundation\Request;



class SearchPageController extends Controller
{

    public function basicAction(Request $request)
    {
	$search = new Search();
		$form = $this->createForm(new HomeSearch(), $search);
	    $request = $this->getRequest();
		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
		 return $this->redirect($this->generateUrl('strathPrimomBundle_homepage'));
			}
		}	

	$sanitisedString = $getArr['hsearch']['searchString'];
		$sanitisedString =str_replace('/','', $sanitisedString);

	$sanitisedString =str_replace(' ','+', $sanitisedString);
	$searchString= "http://suprimo.lib.strath.ac.uk:1701/PrimoWebServices/xservice/search/brief?onCampus=false&institution=SU&loc=local,scope:(SU)&query=any,contains,$sanitisedString&bulkSize=10&indx=$_pg";

	$data = file_get_contents("$searchString",0);
	   
	$sxe = simplexml_load_string($data);
		
		if ($sxe === false) {
		   echo 'Error while parsing the document';

		}

	// grab some info about search and display
	$totalhits=	$sxe->xpath("//@TOTALHITS"); 
	$firsthit= 	$sxe->xpath('//@FIRSTHIT');
	$lasthit= 	$sxe->xpath('//@LASTHIT');

	$collText= "Vsiewing $firsthit[0] to $lasthit[0] of $totalhits[0]";

	//loop, grab data from the XML and display it
	foreach ($sxe->xpath('//sear:DOC') as $doc) {
	$result['title']= 			$doc->PrimoNMBib->record->display->title;
	$result['creator']= 		$doc->PrimoNMBib->record->display->creator;
    $result['publisher']= 		$doc->PrimoNMBib->record->display->publisher;	    	
    $result['creationDate']= 	$doc->PrimoNMBib->record->display->creationdate;	    	
	$result['format']= 			$doc->PrimoNMBib->record->display->format;
	$result['version'] = 		$doc->PrimoNMBib->record->display->version;
	$result['frbrgroupid'] = 	$doc->PrimoNMBib->record->facets->frbrgroupid;
	$result['recordid'] = 		$doc->PrimoNMBib->record->control->recordid;
	$results[] = $result;
    }
	
	 return $this->render('strathPrimomBundle:Search:basic.html.twig', array(
	 		'form' => $form->createView(),
            'results' => $results,
			'collText' => $collText,
			));

}
}


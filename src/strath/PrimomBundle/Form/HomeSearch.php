<?php

namespace strath\PrimomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HomeSearch extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('searchString','text',array(
						'label'=> 'Enter search term below and click submit:',));
    }

    public function getName()
    {
        return 'hsearch';
    }
}
<?php

namespace strath\PrimomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Form;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('barcode');
		$builder->add('familyName');

/*	$builder->addEventlistener(
			FormEvents::PRE_SUBMIT,
			function(FormEvent $event){
				$data = $event->getData();
				$form = $event->getForm();
				$xml = '<?xml version="1.0" encoding="UTF-8"?><ser:serviceParameters xmlns:ser="http://www.endinfosys.com/Voyager/serviceParameters"><ser:parameters></ser:parameters><ser:patronIdentifier lastName="'.$data['familyName'].'"><ser:authFactor type="B">'.$data['barcode'].'</ser:authFactor></ser:patronIdentifier></ser:serviceParameters>';
				$event->setData(array('barcode'=> $xml, 'familyName' => 'Butler'));
			}
		); */
    }

    public function getName()
    {
        return 'login';
    }
}